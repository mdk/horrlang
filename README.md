# Horrlang is an esoteric language, you should not try it.

In Horrlang, memory and instructions are mixed, so you can use old instructions
as new memory places, or do some metaprogramming rewriting your code as it runs.

Just like in LISP but ... without lists ... good luck ...

In Horrlang you have basically 2 pointers, a memory one and an instruction
one. Both start at 0. The instruction one follows instructions that can move
the memory pointer. Have a look at the instruction set:

Instruction set:

- Lx: `left`: Move memory pointer on the left for `x` chars (defaults to `1`).
- Rx: `right`: Move memory pointer on the right for `x` chars (defaults to `1`).
- Nx: `next`: Forward Jump: Move execution pointer to the first found `x` on the left of its current position.
- Px: `prev`: Backward Jump: Move execution pointer to the first found `x` on the right of its current position.
- O: `output`: Copy the current value under memory pointer to stdout.
- I: `input`: Copy from stdin to the current memory pointer.
- K: Decrement the value under the current memory pointer.
- H: Increment the value under the current memory pointer.
- Jx: `jump` If the value under the current memory pointer is not `0` (0x30): Move the instruction pointer to the previous `x` instruction (like `Px`). (Jumps to the beginning if no `x` char is found).

The Hello World:

    python horrlang.py 'R9R9R9R4OROROROROROROROROROROROHello World.'

Or:

    python horrlang.py 'Hello World0KORJK'

Writing a infinite serie of P:

    R5OP5P or R3OP3

Write 123456789 using a loop:

    R9R3:ROHLKJ:81
