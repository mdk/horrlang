from horrlang import eval


def test_hello_world():
    assert eval("R9R9R9R4OROROROROROROROROROROROHello World.") == "Hello World."


def test_short_hello_world():
    assert eval("Hello World0KORJK") == "Hello World"


def test_from_readme():
    assert eval("R5OP5P", max_cycles=100) == "PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP"


def test_from_readme_short():
    assert eval("R3OP3", max_cycles=100) == "PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP"


def test_123456789():
    assert eval("R9R3:ROHLKJ:91") == "123456789"
